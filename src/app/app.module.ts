import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// APP 
import { AppComponent } from './app.component';
import { RoutingModule } from './app.routing.module';
import { MaterialModule } from './app.materials.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NavbarComponent } from './Template/navbar/navbar.component';
import { HeaderComponent } from './template/header/header.component';
import { FooterComponent } from './template/footer/footer.component';

import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';


@NgModule({
  // LEARN: used to make declare (including components and pipes) from the current module available to other directives in the current module. 
  //        Selectors of directives, components or pipes are only matched against the HTML if they are declared or imported
  declarations: [
    AppComponent,
    HeaderComponent, NavbarComponent, FooterComponent,
    
    HomeComponent,
    AboutUsComponent
  ],

  // LEARN: used to import supporting files likes FormsModule, RouterModule, CommonModule, or any other custom-made feature module.
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutingModule,
    MaterialModule
  ],

  // LEARN: used to inject the services required by components, directives, pipes
  providers: [],

  // LEARN: Identifies the root component that Angular should bootstrap when it starts the application.
  bootstrap: [AppComponent]
})
export class AppModule { }
