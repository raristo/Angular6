import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// LEARN: non-lazy loading 
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';

const routes: Routes = [
  { path: 'aboutus', component: AboutUsComponent },
  { path: '', component: HomeComponent },
];

export  const RoutingModule: ModuleWithProviders = RouterModule.forRoot(routes);
